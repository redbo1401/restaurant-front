import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProduitAddComponent } from './produit-add/produit-add.component';
import { ProduitListComponent } from './produit-list/produit-list.component';



@NgModule({
  declarations: [ProduitAddComponent, ProduitListComponent],
  imports: [
    CommonModule
  ]
})
export class ProduitModule { }
